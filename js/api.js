function getQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return decodeURI(r[2]);
	return null;
}

let baseUrl = 'https://testtc.chuangshengjiaoyu.com/';
//分享报告数据接口
let shareInfoApi = baseUrl + 'api/report/shareInfo';

//分享练习单
let exerciseInfoApi = baseUrl + 'api/Check/exerciseInfo';

//班级口语报告
let spokenDataApi = baseUrl + 'api/spoken/spokenDataHtml';

//考试报告试卷分析-总成绩分析接口
let scoreAnalyApi = baseUrl + 'api/testing/scoreAnaly';

//考试报告学生成绩接口
let studentListApi = baseUrl + 'api/testing/studentList';

//考试报告试卷分析-成绩分布接口
let scoreDistriApi = baseUrl + 'api/testing/scoreDistri';

//考试报告试卷分析-考试考勤接口
let scoreAttendanceApi = baseUrl + 'api/testing/scoreAttendance';

//考试报告试卷分析-成绩分析接口
let scoreAnalysisApi = baseUrl + 'api/testing/scoreAnalysis';

//考试报告题目分析详情接口
let testingAnalyApi = baseUrl + 'api/testing/testingAnaly';

//具体题目详情+学生列表接口
let contentStuListApi = baseUrl + 'api/testing/contentStuList';

//学生成绩个人学生答题情况查看(模考报告)接口
let personReportApi = baseUrl + 'api/testing/personReport';

/**
 * 将秒转换为 分:秒
 * s int 秒数
 */
function s_to_hs(s) {
	//计算分钟
	//算法：将秒数除以60，然后下舍入，既得到分钟数
	var h;
	h = Math.floor(s / 60);
	//计算秒
	//算法：取得秒%60的余数，既得到秒数
	s = s % 60;
	//将变量转换为字符串
	h += '';
	s += '';
	//如果只有一位数，前面增加一个0
	h = (h.length == 1) ? '' + h : h;
	s = (s.length == 1) ? '' + s : s;
	return h + '分' + s + '秒';
}

/**
 * 时间錯转换
 */
function formatUnixtimestamp(unixtimestamp) {
	var unixtimestamp = new Date(unixtimestamp * 1000);
	var year = 1900 + unixtimestamp.getYear();
	var month = "0" + (unixtimestamp.getMonth() + 1);
	var date = "0" + unixtimestamp.getDate();
	var hour = "0" + unixtimestamp.getHours();
	var minute = "0" + unixtimestamp.getMinutes();
	var second = "0" + unixtimestamp.getSeconds();
	return year + "-" + month.substring(month.length - 2, month.length) + "-" + date.substring(date.length - 2, date.length);
	// 			 return year + "-" + month.substring(month.length-2, month.length)  + "-" + date.substring(date.length-2, date.length)
	// 			    + " " + hour.substring(hour.length-2, hour.length) + ":"
	// 			    + minute.substring(minute.length-2, minute.length) + ":"
	// 			    + second.substring(second.length-2, second.length);
}

function awakenApp(url) {
	var timeout, t = 1000,
		hasApp = true;
		if (browser.versions.ios) {
			window.location.href = url; //ios下载地址
		}
	setTimeout(function() {
		if (!hasApp) {
			//未安装app
			if (browser.versions.ios) {
				window.location.href = 'https://itunes.apple.com/cn/app/id1373149692?mt=8'; //ios下载地址
			} else {
				window.location.href = 'https://a.app.qq.com/o/simple.jsp?pkgname=com.zaidayou.newcs'; //安卓下载地址
			}
		}
		document.body.removeChild(ifr);
	}, 2000)

	var t1 = Date.now();
	var ifr = document.createElement("iframe");
	ifr.setAttribute('src', url);
	ifr.setAttribute('style', 'display:none');
	document.body.appendChild(ifr);
	timeout = setTimeout(function() {
		var t2 = Date.now();
		if (!t1 || t2 - t1 < t + 100) {
			hasApp = false;
		}
	}, t);


}
//判断访问终端
var browser = {
	versions: function() {
		var u = navigator.userAgent,
			app = navigator.appVersion;
		return {
			trident: u.indexOf('Trident') > -1, //IE内核
			presto: u.indexOf('Presto') > -1, //opera内核
			webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
			gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
			mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
			ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
			android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
			iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
			iPad: u.indexOf('iPad') > -1, //是否iPad
			webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
			weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
			qq: u.match(/\sQQ/i) == " qq" //是否QQ
		};
	}(),
	language: (navigator.browserLanguage || navigator.language).toLowerCase()
}


function numberCase(number){
	if(number ==1){
		return "一";
	}else if(number ==2){
		return "二";
	}else if(number ==3){
		return "三";
	}else if(number ==4){
		return "四";
	}else if(number ==5){
		return "五";
	}else if(number ==6){
		return "六";
	}else if(number ==7){
		return "七";
	}else if(number ==8){
		return "八";
	}else if(number ==9){
		return "九";
	}
}


function Base64() {
 
    // private property
    _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
 
    // public method for encoding
    this.encode = function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = _utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
            _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
            _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
        }
        return output;
    }
 
    // public method for decoding
    this.decode = function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = _keyStr.indexOf(input.charAt(i++));
            enc2 = _keyStr.indexOf(input.charAt(i++));
            enc3 = _keyStr.indexOf(input.charAt(i++));
            enc4 = _keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = _utf8_decode(output);
        return output;
    }
 
    // private method for UTF-8 encoding
    _utf8_encode = function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
 
        }
        return utftext;
    }
 
    // private method for UTF-8 decoding
    _utf8_decode = function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while ( i < utftext.length ) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}