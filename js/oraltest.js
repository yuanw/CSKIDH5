//-----------------------------VUE 接口访问-------------------------------
new Vue({
	el: '#message',
	data: {
		params: getQueryString("params"),
		avg_score: '',
		max_score: '',
		min_score: '',
		scoring_rate: '',
		standard_deviation: '',
		total_score: '',
		variance: '',
		baseInfo: [],
		stulist: [],
		scoreDistriInfo: [],
		complete: [],
		notsub: [],
		notjoin: [],
		examdetails: [],
		task_id : '',

	},
	mounted: function() {
		this.isterminal();
		if (browser.versions.weixin) {
			document.getElementById('JweixinTip').style.display = 'block';
		}
		//考试报告试卷分析-总成绩分析
		this.scoreAnaly();
		//考试报告学生成绩接口
		this.studentList();
		//考试报告试卷分析-成绩分布接口
		this.scoreDistri();
		//考试报告试卷分析-考试考勤接口
		this.scoreAttendance();
		//考试报告试卷分析-成绩分析接口
		this.scoreAnalysis();
		this.testingAnaly();
	},
	methods: {
		//判断 android ios
		isterminal: function() {
			var that = this;
			if (that.isTerminal == 'android') {
				that.isTerminal = 1;
			} else if (that.isTerminal == 'ios') {
				that.isTerminal = 2;
			}
		},
		scoreAnaly: function() {

			var b = new Base64();
			var str = b.decode(this.params);
			var objParams = JSON.parse(str);
			// console.log(objParams);
			this.terminal = objParams.terminal;
			this.task_id = objParams.task_id;
			//分享报告请求方法
			this.$http.post(scoreAnalyApi, objParams, {
				emulateJSON: true
			}).then(function(res) {
				if (res.data.error_code == 200) {
					//总成绩
					let scoreAnaly = res.data.data;
					this.avg_score = scoreAnaly.avg_score;
					this.max_score = scoreAnaly.max_score;
					this.min_score = scoreAnaly.min_score;
					this.scoring_rate = scoreAnaly.scoring_rate;
					this.standard_deviation = scoreAnaly.standard_deviation;
					this.total_score = scoreAnaly.total_score;
					this.variance = scoreAnaly.variance;

					initScoreChart(scoreAnaly);

				} else {
					//请求错误
					window.android.errorMsg(res.data.info);
				}
			}).catch(e => {
				// 打印一下错误
				console.log(e);
			})
		},
		studentList: function() {

			var b = new Base64();
			var str = b.decode(this.params);
			var objParams = JSON.parse(str);
			// console.log(objParams);
			this.terminal = objParams.terminal;

			//分享报告请求方法
			this.$http.post(studentListApi, objParams, {
				emulateJSON: true
			}).then(function(res) {
				if (res.data.error_code == 200) {
					//总成绩
					let studentList = res.data.data;
					this.baseInfo = studentList.baseInfo;
					this.stulist = studentList.list;

				} else {
					//请求错误
					window.android.errorMsg(res.data.info);
				}
			}).catch(e => {
				// 打印一下错误
				console.log(e);
			})
		},
		scoreDistri: function() {
			var b = new Base64();
			var str = b.decode(this.params);
			var objParams = JSON.parse(str);
			this.terminal = objParams.terminal;
			//考试报告试卷分析-成绩分布
			this.$http.post(scoreDistriApi, objParams, {
				emulateJSON: true
			}).then(function(res) {
				if (res.data.error_code == 200) {
					this.scoreDistriInfo = res.data.data;
					initHighcharts(res.data.data);

				} else {
					//请求错误
					window.android.errorMsg(res.data.info);
				}
			}).catch(e => {
				// 打印一下错误
				console.log(e);
			})
		},
		scoreAttendance: function() {
			var b = new Base64();
			var str = b.decode(this.params);
			var objParams = JSON.parse(str);
			console.log(objParams);
			this.terminal = objParams.terminal;
			//考试报告试卷分析-成绩分布
			this.$http.post(scoreAttendanceApi, objParams, {
				emulateJSON: true
			}).then(function(res) {
				if (res.data.error_code == 200) {
					this.complete = res.data.data.complete;
					this.notsub = res.data.data.notsub;
					this.notjoin = res.data.data.notjoin;

				} else {
					//请求错误
					window.android.errorMsg(res.data.info);
				}
			}).catch(e => {
				// 打印一下错误
				console.log(e);
			})
		},
		scoreAnalysis: function() {
			var b = new Base64();
			var str = b.decode(this.params);
			var objParams = JSON.parse(str);
			this.terminal = objParams.terminal;
			//考试报告试卷分析-成绩分布
			this.$http.post(scoreAnalysisApi, objParams, {
				emulateJSON: true
			}).then(function(res) {
				if (res.data.error_code == 200) {
					histogram(res.data.data);
				} else {
					//请求错误
					window.android.errorMsg(res.data.info);
				}
			}).catch(e => {
				// 打印一下错误
				console.log(e);
			})
		},
		testingAnaly: function() {
			var b = new Base64();
			var str = b.decode(this.params);
			var objParams = JSON.parse(str);
// 			console.log("-----------------------");
// 			console.log(scoreAnalysisApi)
// 			console.log(objParams);
// 			console.log("-----------------------");
			this.terminal = objParams.terminal;
			//考试报告试卷分析-成绩分布
			this.$http.post(testingAnalyApi, objParams, {
				emulateJSON: true
			}).then(function(res) {
				if (res.data.error_code == 200) {
					this.examdetails = res.data.data;
					console.log(this.examdetails.list)
				} else {
					//请求错误
					window.android.errorMsg(res.data.info);
				}
			}).catch(e => {
				// 打印一下错误
				console.log(e);
			})
		},
		click_x5: function() {
			if (browser.versions.android) {
				console.log('-------------------打印成功，走了调用安卓分享---------------------');
				AndroidBack.onX5ButtonClicked();
			}
		},
		notjoinStu: function() {
			if (browser.versions.android) {
				AndroidBack.workAttendance(this.task_id);
			}else{
				window.webkit.messageHandlers.jsCallOc.postMessage("missInfo");
			}
		},
		forId:function(items,itemDatails){
			return "indicatorContainer_"+items.id+"_"+itemDatails.num+"_"+itemDatails.question_id
		},
		quseScoreDeatils:function(items,itemDatails){
			if (browser.versions.android) {
				AndroidBack.quseScoreDeatils(this.task_id,items.id,itemDatails.question_id);
			}else{
				var objParams  = {
					task_id : this.task_id,
					id : items.id,
					question_id : itemDatails.question_id
				}
				window.webkit.messageHandlers.examInfo.postMessage(objParams);
			}
		},stuDetails:function(item){
			console.log(item)
			if (browser.versions.android) {
				AndroidBack.stuDetails(this.task_id,item.uid);
			}else{
				var objParams  = {
					task_id : this.task_id,
					uid : item.uid
				}
				window.webkit.messageHandlers.userInfo.postMessage(objParams);
			}
		}

	},
	filters: {
		numFilter(value) {
			// 截取当前数据到小数点后三位
			let transformVal = parseFloat(value).toFixed(3)
			let realVal = transformVal.substring(0, transformVal.length - 1)
			if (realVal == 'NaN') {
				realVal = 0;
			}
			// num.toFixed(3)获取的是字符串
			return parseFloat(realVal)
		},
		numLawFilter([num, count]) {
			let law = num / count * 100;
			// 截取当前数据到小数点后三位
			let transformVal = parseFloat(law).toFixed(0);

			if (transformVal == 'NaN') {
				transformVal = 0;
			}

			// console.log(transformVal);

			//     let realVal = transformVal.substring(0, transformVal.length - 1)
			// num.toFixed(3)获取的是字符串
			return parseFloat(transformVal)
		},
		formatUnixtimestamp(value) {
			return formatUnixtimestamp(value);
		},
		formatSToHs(value) {
			if(value == 1000000000000000){
				return "";
			}else{
				return s_to_hs(value);
			}
		},
		transformValNot([stuScore,score ]) {
			return transformValNot(stuScore,score);
		},
		initProgress([items,itemDatails]) {
			var id = "indicatorContainer_"+items.id+"_"+itemDatails.num+"_"+itemDatails.question_id
			var value = itemDatails.score_rate_detail * 100;
			initProgress(id,value);
		},
		scoreRate(score) {
			var value = score * 100;
			// 截取当前数据到小数点后三位
			let transformVal = parseFloat(value).toFixed(2)
			let realVal = transformVal.substring(0, transformVal.length - 1)
			if (realVal == 'NaN') {
				realVal = 0;
			}
			// num.toFixed(3)获取的是字符串
			return parseFloat(realVal);
		}

	}
});
//-----------------------------VUE 接口访问 ent-------------------------------


//-----------------------------初始化3D饼图-------------------------------
function initHighcharts(scoreDistri) {
	// console.log(scoreDistri);
	var excellent = scoreDistri.excellent;
	var good = scoreDistri.good;
	var qualified = scoreDistri.qualified;
	var difference = scoreDistri.difference;
	var count = scoreDistri.count;

	var chart = Highcharts.chart('exam-container', {
		chart: {
			type: 'pie',
			options3d: {
				enabled: true,
				alpha: 45
			}
		},
		navigation: {
			buttonOptions: {
				enabled: false
			}
		},
		credits: {
			enabled: false
		},
		title: {
			text: ''
		},
		plotOptions: {
			pie: {
				innerSize: 90,
				depth: 30,
			}
		},
		series: [{
			name: '',
			data: [
				["<span style='color:#ffca4a'>合格" + transformVal(qualified, count) + "</span>", qualified],
				["<span style='color:#ff4a64'>待合格" + transformVal(difference, count) + "</span>", difference],
				["<span style='color:#4581db'>良好" + transformVal(good, count) + "</span>", good, ],
				["<span style='color:#36c86b'>优秀" + transformVal(excellent, count) + "</span>", excellent]
			],
			colors: ['#ffca4a', '#ff4a64', '#4581db', '#36c86b']
		}]
	});
}

//-----------------------------初始化3D饼图 ent-------------------------------


//-----------------------------初始化柱状图-------------------------------
function histogram(histogramData) {
	// 			var strData = "";
	Math.easeOutBounce = function(pos) {
		if ((pos) < (1 / 2.75)) {
			return (7.5625 * pos * pos);
		}
		if (pos < (2 / 2.75)) {
			return (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
		}
		if (pos < (2.5 / 2.75)) {
			return (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
		}
		return (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
	};

	var data1 = 0;
	var data2 = 0;
	var data3 = 0;
	var data4 = 0;

	for (var i = 0; i < histogramData.length; i++) {
		if (histogramData[i].stem_type == 1) {
			data1 = transformValNot(histogramData[i].stu_score, histogramData[i].score);
		} else if (histogramData[i].stem_type == 2) {
			data2 = transformValNot(histogramData[i].stu_score, histogramData[i].score);
		} else if (histogramData[i].stem_type == 3) {
			data3 = transformValNot(histogramData[i].stu_score, histogramData[i].score);
		} else if (histogramData[i].stem_type == 4) {
			data4 = transformValNot(histogramData[i].stu_score, histogramData[i].score);
		}
	}
	Highcharts.chart('histogram-container', {
		chart: {
			type: 'column'
		},
		navigation: {
			buttonOptions: {
				enabled: false
			}
		},
		credits: {
			enabled: false
		},
		title: {
			text: ''
		},
		xAxis: {
			type: 'category'
		},
		yAxis: {
			title: {
				text: ''
			},
			max: 100
		},
		legend: {
			enabled: false
		},
		plotOptions: {
			series: {
				animation: {
					duration: 2000,
					easing: 'easeOutBounce'
				},
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '<span style="font-size:11px;color:#4581db">{point.y:.1f}%</span>'
				},
				color: '#FF0000',
				fillColor: {
					stops: [
						[0, Highcharts.getOptions().colors[0]],
						[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					]
				}
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:11px">{series.data.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
		},
		series: [{
			name: '',
			colorByPoint: true,
			data: [{
				name: '单词朗读 ',
				y: data1,
				color: {
					linearGradient: {
						x1: 0,
						x2: 0,
						y1: 0,
						y2: 1
					},
					stops: [
						[0, Highcharts.getOptions().colors[0]],
						[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					]
				}
			}, {
				name: '单词朗读',
				y: data2,
				color: {
					linearGradient: {
						x1: 0,
						x2: 0,
						y1: 0,
						y2: 1
					},
					stops: [
						[0, Highcharts.getOptions().colors[0]],
						[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					]
				}
			}, {
				name: '回答问题',
				y: data3,
				color: {
					linearGradient: {
						x1: 0,
						x2: 0,
						y1: 0,
						y2: 1
					},
					stops: [
						[0, Highcharts.getOptions().colors[0]],
						[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					]
				}
			}, {
				name: '短文朗读',
				y: data4,
				color: {
					linearGradient: {
						x1: 0,
						x2: 0,
						y1: 0,
						y2: 1
					},
					stops: [
						[0, Highcharts.getOptions().colors[0]],
						[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					]
				}
			}]
		}]
	});
}

//-----------------------------初始化柱状图 ent-------------------------------


function transformVal(num, count) {
	let law = num / count * 100;
	// 截取当前数据到小数点后三位
	let transformVal = parseFloat(law).toFixed(0);

	if (transformVal == 'NaN') {
		transformVal = 0;
	}
	// console.log(transformVal);
	return transformVal + "%";
}

function transformValNot(num, count) {
	let law = num / count * 100;
	// 截取当前数据到小数点后三位
	let transformVal = parseFloat(law).toFixed(0);

	if (transformVal == 'NaN') {
		transformVal = 0;
	}
	return Number(transformVal);
}


//-----------------------------初始化环形饼图-------------------------------
function initScoreChart(scoreData) {
	console.log(scoreData)
	var maxScore = Number(scoreData.max_score) > 0 ? Number(scoreData.max_score) : 0;
	var minScore = Number(scoreData.min_score) > 0 ? Number(scoreData.min_score) : 0;
	var agvScore = Number(scoreData.avg_score) > 0 ? Number(scoreData.avg_score) : 0;

	var maxScoreData = [];
	var minScoreData = [];
	var avgScoreData = [];

	maxScoreData = [{
		name: "最高分",
		y: maxScore,
		color: {
			linearGradient: {
				x1: 1,
				y1: 1,
				x2: 1,
				y2: 0
			},
			stops: [
				[0, '#3ac86e'],
				[1, Highcharts.Color('#3ac86e').setOpacity(0).get('rgba')]
			]
		}
	}, {
		name: "",
		y: (100 - maxScore),
		color: "#ffffff"
	}];
	avgScoreData = [{
		name: "平均分",
		y: agvScore,
		color: {
			linearGradient: {
				x1: 1,
				y1: 1,
				x2: 1,
				y2: 0
			},
			stops: [
				[0, '#4d86dd'],
				[1, Highcharts.Color('#4d86dd').setOpacity(0).get('rgba')]
			]
		}
	}, {
		name: "",
		y: (100 - agvScore),
		color: "#ffffff"
	}];
	minScoreData = [{
		name: "最低分",
		y: minScore,
		color: {
			linearGradient: {
				x1: 1,
				y1: 1,
				x2: 1,
				y2: 0,
			},
			stops: [
				[0, '#ff4a64'],
				[1, Highcharts.Color('#ff4a64').setOpacity(0).get('rgba')]
			]
		}
	}, {
		name: "",
		y: (100 - minScore),
		color: "#ffffff"
	}];

	// tooltip.enabled = false;
	// Create the chart
	Highcharts.chart('score-container', {
		chart: {
			type: 'pie'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: ''
		},
		yAxis: {
			title: {
				text: ''
			}
		},
		navigation: {
			buttonOptions: {
				enabled: false
			}
		},
		credits: {
			enabled: false
		},
		plotOptions: {
			pie: {
				shadow: false,
				center: ['50%', '50%']
			}
		},
		tooltip: {
			enabled: false,
			valueSuffix: '%'
		},
		series: [{
			name: '最高分',
			data: maxScoreData,
			size: '90%',
			innerSize: '80%',
			dataLabels: {
				formatter: function() {
					// display only if larger than 1

					if (this.point.name == '') {
						return null;
					} else {
						return this.y > 1 ? "<b style='color:#3ac86e'  class='f30' >" + this.y + "</b> " +
							"<b style='color:#3ac86e' >" + this.point.name + "</b>" : null;
					}

				}
			},
			id: 'max_score'
		}, {
			name: '平均分',
			data: avgScoreData,
			size: '70%',
			innerSize: '75%',
			dataLabels: {
				formatter: function() {
					// display only if larger than 1
					if (this.point.name == '') {
						return null;
					} else {
						return this.y > 1 ? "<b style='color:#4d86dd'  class='f30' >" + this.y + "</b> " +
							"<b style='color:#4d86dd' >" + this.point.name + "</b>" : null;
					}
				}
			},
			id: 'avg_score'
		}, {
			name: '最低分',
			data: minScoreData,
			size: '50%',
			innerSize: '80%',
			dataLabels: {
				formatter: function() {
					// display only if larger than 1
					if (this.point.name == '') {
						return null;
					} else {
						return this.y > 1 ? "<b style='color:#ff4a64'  class='f30' >" + this.y + "</b> " +
							"<b style='color:#ff4a64' >" + this.point.name + "</b>" : null;

					}
				},
			},
			id: 'min_score'
		}],
		responsive: {
			rules: [{
				condition: {
					maxWidth: 400
				},
				chartOptions: {
					series: [{
						id: 'min_score',
						dataLabels: {
							enabled: true
						}
					}, {
						id: 'avg_score',
						dataLabels: {
							enabled: true
						}
					}, {
						id: 'max_score',
						dataLabels: {
							enabled: true
						}
					}]
				}
			}]
		}
	});
}

//-----------------------------初始化环形饼图ent-------------------------------
function initProgress(id,value){
	$("#"+id).radialIndicator({
		barColor: {
			0: '#FF0000',
			30: '#FF0000',
			59: '#FF0000',
			60: '#36c86b',
			100: '#36c86b'
		},
		barWidth: 5,
		initValue: value,
		roundCorner: true,
		percentage: true
	});
}


var swiper = new Swiper('.swiper-container', {
	on: {
		slideChangeTransitionStart: function() {
			$(".tabindexList span").removeClass("color5").eq(this.activeIndex).addClass("color5");
		}
	}
});
$(".tabindexList span").click(function() {
	var index = $(this).index();
	$(".tabindexList span").removeClass("color5").eq(index).addClass("color5");
	swiper.slideTo(index, 300, false);
})
